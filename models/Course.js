/*
    Naming convention for model files is singular and capitalized form
*/

const mongoose = require("mongoose");

/* 
    Mongoose Schema

    Before we can create documents from our API to save into our database. First is to determine the structure of the documents to be written in the database.

    Schema acts a a blueprint for our data/document.
    Schema is a representation of how the document is structured. It also determines the types of data and the expected properties.

    So with this, we won't have to worry for had input "stock/s" on our documents, because we can make it uniform with a schema.

    In mongoose, to create a schema, we use the Schema() constructor from mongoose. Allows to create new mongoose schema object.

*/

const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    enrollees: [
        {
            userId: {
                type: String,
                required: [true, "UserID is required"]
            },
            dateEnrolled: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        }
    ]
});

// module.exports - so we can import and use this fine in another file
/* 
    Mongoose Model

    Is the connection to our collection

    It has 2 arguments
    1st: name of the collection of the model is going to connect to. In mongoDB, once we create a new course document this model will connect to our courses collection. But since the course collection is not yet created initially, mongo will create it.

    2nd: the schema of the documents in the collection
*/
module.exports = mongoose.model("Course", courseSchema);