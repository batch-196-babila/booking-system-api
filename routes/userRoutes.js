const express = require("express");
const router = express.Router();

// In fact, routes should just contain the endpoints and it should only trigger the function but it should not where we define the logig of our function

// The business logic of our API should be in controllers

const userControllers = require("../controllers/userControllers");

// check the imported userControllers
//console.log(userControllers);

// import auth to be bale to have access and use the verify methos to act as middleware for our routes
// Middlewares add in the route such as verify() will have access to the req,res objects

const auth = require("../auth");

// destructure auth to get only our methods and save it in variables
const {verify} = auth; // only the module.exports.verify from auth.js will be used

/*
    Updated Route Syntax
    method("/endpoint", registerUser)
*/

// register
router.post('/', userControllers.registerUser);

// get specific details
// POST method to get user details by ID
// http...4000/users/details

// verify() is used as middleware means our req will het through verify first before our controller

// verify() will not only check the validity of the token but also add the decoded data of the token in the request object as req.user

router.get('/details', verify, userControllers.getUserDetails);

// Route for User Authentication
router.post('/login', userControllers.loginUser);

router.post('/checkEmail', userControllers.checkEmail);

// User Enrollment
// courseID will come from the req/body
// userId will come from req.user

router.post('/enroll', verify, userControllers.enroll);

module.exports = router;