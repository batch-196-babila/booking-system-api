/*
    To be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use anotehr method from express to contain our routes

    the Router() method allows us to contain routes
*/

const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth")
const { verify, verifyAdmin } = auth;

// gets all course
router.get('/', verify, verifyAdmin, courseControllers.getAllCourses);

// end point /course
// Only logged in user is able to use addCourse
// verifyAdmin() will disallow regular logged in and non-logged in users from using route
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

// Get all active courses (regular, non-logged in users)
router.get('/activeCourses', courseControllers.getActiveCourses);


// get method should not have a request body

// we can pass data in a route without the use of request body by passing a small amount of data through the url with the use of route params
// /endpoint/:routeParams
// http...4000/courses/getSingleCourse/<id>
router.get('/getSingleCourse/:courseId', courseControllers.getSingleCourse);

// update a single course
// pass the id course to update via route param
// the update details will be passed via request body

router.put('/updateCourse/:courseId', verify, verifyAdmin, courseControllers.updateCourse);

// archive a single course = soft delete - not actual delete
// pass the id for the course we want to update via route params
// we will directly update the course as inactive

router.delete("/archiveCourse/:courseId", verify, verifyAdmin, courseControllers.archiveCourse);

module.exports = router;