/* Auth.js is our own module which will contain methods to help authorized or restrict users from accessing certain features in our application */

const jwt = require("jsonwebtoken");

// This is the secret string which will validate or which will use to check the validity of passed token. If token does not contain this secret string, then that token is invalid.
const secret = "courseBookingAPI";

/* 
    JWT is a way to securely pass info from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access
    
    JWT is like a gift wrapping service which will encode the user's details and can only be unwrapped by hwt's own methods and if the secret is intact

    If the jwt seemed tampwered will reject the user's attempt to access feature in our app
*/

module.exports.createAccessToken = (userDetails) => {
    // Pick only certain details from our user to be included in the token
    // Password should not be included

    //console.log(userDetails);
    const data = {
        id: userDetails.id,
        email: userDetails.email,
        isAdmin: userDetails.isAdmin
    }
    console.log(data);

    // jwt.sign() will create a JWT using our data object, with our secret
    // {} deault algorith to create our token
    return jwt.sign(data, secret,{});
}

module.exports.verify = (req, res, next) => {
    // verify is used to be a middleware, wherein it will be added per route to act as a gate to check if token being passed is valued or not
    // Also allow us to check if user is allowed to access the feature of the app or not
    // Check the validitiy of the token using its secret

    // we will pass the token with our request headers as authorization
    // Requests that need a token must be able to pass the token in the authorization headers
    let token = req.headers.authorization;

    // If token is undefined, then req.headers.authorization is empty. Which means the request did not pass a token in the authorization headers.
    if (typeof token === "undefined") {
        return res.send({auth: "Failed. No token."})
    } 
    else {

        // When passing JWT we use the Bearer Token authorization. This means that when JWT is passed a word "Bearer" as well as space is added

        // slice() and copy the rest of the token without the word Bearer
        // slice(<startingPosition>, <endPosition>)
        // Update token with the sliced version

        // console.log(token); token with bearer word
        token = token.slice(7);
        // console.log(token); token without bearer word

        // verify the validity of a token by checking the overall length of the token and if the token contains the secret
        // It has 3 arguments the token, secret and a handler function which will handle iether error if the token is invalid or decoded data from the token
        jwt.verify(token, secret, function(err, decodedToken) {
            // console.log(decodedToken); // contains the data of the token if the token is verified
            // console.log(err); // null as no error

            // send message to our client if there is an error or add our decodedToken to our requestObject which we can pass to the next controller/middleware

            if(err) {
                return res.send({
                    auth: "Failed",
                    message: err.message
                })
            }
            else {
                // add a new user property in the request object and add the decoded token as its value
                // The next controller/middleware will now have access to the id, email and isAdmin properties of the logged in user
                req.user = decodedToken;

                // next() this will let us proceed to the next middleware or controller.
                next();
            }
        })
    }
}

// verifyAdmin will be used as a middleware
// It has to follow or be added after verify(), so we can check for the validity and add the decodedToken is the request object as req.user

module.exports.verifyAdmin = (req, res, next) => {
    // verifyAdmin must be executed after verify to have access to req.user
    console.log(req.user); 

    // check if user is admin to run next() method
    if (req.user.isAdmin) {
        next(); // proceed to next middleware or controller
    } 
    else {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden"
        })
    }
} 