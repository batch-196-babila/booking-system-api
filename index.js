const express = require("express");
// Mongoose is an ODM library to let our ExpressJS API manipulate a MongoDB database
const mongoose = require("mongoose");

const { request } = require("http");
const app = express();
app.use(express.json());
const port = 4000;

/*
    Mongoose Connection
    mongoose.connect() is a method to connect our api with our mongoDB database via the use of mongoose. It has 2 argments
    1st: connection string to conect our api to mongoDB
    2nd: is an object used to add information between mongoose and mongoDB

    replace/change <password> in the connection string to your db user password

    just before the ? in the connection string, add the database name
*/

// bookingAPI in the link was added
mongoose.connect("mongodb+srv://admin:admin123@cluster0.5eaawok.mongodb.net/bookingAPI?retryWrites=true&w=majority", 
{
    useNewURLParser: true,
    useUnifiedTopology: true
});

// Create notifications if the connection to the db is a success or failed
let db = mongoose.connection;

// Show notifications of an internal server error from MongoDB
db.on('error', console.error.bind(console, "MongoDB Connection Error"));

// If connection is successful, output message be displayed in terminal
db.once('open', () => console.log("Connected to MongoDB"));

/*
    import out routes and use it as middleware
    we will be able tog group together our routes
*/

// import course routes

const courseRoutes = require('./routes/courseRoutes');
console.log(courseRoutes);

app.use('/courses', courseRoutes);

// import the user routes

const userRoutes = require("./routes/userRoutes");
app.use('/users', userRoutes);

app.listen(port, () => console.log(`Booking System API running at port 4000`));
