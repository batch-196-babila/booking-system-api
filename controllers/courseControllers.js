// Import the Course model so we can manipulate it and add a new course document

const Course = require("../models/Course");

module.exports.getAllCourses = (req, res) => {
    // res.send("This route will get all course documents");
    // Use the course model to connect to our collections and retrieve our courses
    // To be able to query into our collections we use the model connected to that collection
    // in mongoDB: db.courses.find({})
    // Model.find() returns a collection of documents that matches our criteria similar to mongodb's .find()
    Course.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.addCourse = (req, res) => {
    // console.log(req.body);
    // res.send("This route will create a new course documents");

    // Using the Course model, we will use its constructor to create our Course document which will follow the schema of the model and add methods for document creation

    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    });

    // newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor

    // .save() is added into our newCourse onject. To save the content of newCourse into our collection
    // in mongoDB: db.courses.insertOne();

    // console.log(newCourse);

    // .then() allows us to process the result of a previous function/method in its own function

    // .catch() catches the errors and allows us to process and send the client

    newCourse.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.getActiveCourses = (req, res) => {
    Course.find({isActive: true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

// 
module.exports.getSingleCourse = (req, res) => {

    // req.params is an object that contains the value captured via route params
    // the field name of the req.params indicate the name of the route param
    // console.log(req.params); // dis will display hcourseId: <id num>
    console.log(req.params.courseId); // to get the id passed as the route params

    Course.findById(req.params.courseId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.updateCourse = (req, res) => {
    console.log(req.params.courseId); // check if we get ID

    console.log(req.body); // check the update that is input

    // findByIdAndUpdate - update documents and has 3 arguments
    // findByIdAndUpdate(<id>, {update}, new:true) 
    // new:true returns the updated value bec by default it's the old data

    // we can create a new object to filter update details
    // The indicated fields in the update object fields will be updated
    // fields/property that are not part of the update obkect will not be updated

    let update = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Course.findByIdAndUpdate(req.params.courseId, update, {new: true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.archiveCourse = (req, res) => {
    // console.log(req.params.courseId);

    let softDelete = {
        isActive: false
    }
    
    Course.findByIdAndUpdate(req.params.courseId, softDelete, {new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}
