/*
    Naming convention: named after the model it is concern with

    Controllers are functions which contain the actual business logic of our API and is triggered by a route

    MVC - models, views and contraller
*/

// To create a controller: Add in into our module.exports

// import User model in the controller

const User = require("../models/User");

// import Course
const Course = require("../models/Course");

// import bcrypt here
// package allows us to hash password to add layer of security for our user's details
const bcrypt = require("bcrypt") 

// import auth.js module to use createAccessToken and its subsequent methods
const auth = require("../auth");

module.exports.registerUser = (req, res) => {
    // console.log(req.body); check input
    // using bcrypt to hide the user's password underneath a later of randomized characters. 
    // Salt rounds are the number of times we randomized the string/password hash
    // bcrypt.hashSync(<string>, saltRounds)

    const hashedPw = bcrypt.hashSync(req.body.password, 10);
    // console.log(hashedPw); check 

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo
    });

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

// All routes to courses now has an endpoint prefaced with /courses
// endpoint - /courses/courses

// get userDetails should only allow the LOGGED-in user the get it's own details
module.exports.getUserDetails = (req, res) => {
    // console.log(req.body); // 
    
    // in postman : url:...4000/users/details
    // find() returnn an array of cocuments []
    // User.find({_id: req.body.id}) 
    
    // findOne() return {single document}
    //User.findOne({_id: req.body.id}) 

    // findById() is a mongoose method that will allow us to find a document strictly by its ID

    // This will allow us to ensure that the LOGGED IN USER that PASSED THE TOKEN will be able to get his own details and ONLY its own.

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))   
}

// Login

module.exports.loginUser = (req, res) => {
    console.log(req.body);
/*
    Steps for logging in our user
    1. Find the user by its email
    2. If we found the user: check his password if input password and hash password matches
    3. If we don't find a user, send a message to client
    4. Generate a key for user to have authorization to access certain features in our app.
*/
    // mongoDB: db.users.findOne({email:})
    User.findOne({email: req.body.email})
    .then(foundUser => {
        // foundUser is the parameter that contains the result of findOne
        // findOne() returns null
        if(foundUser === null) {
            return res.send({message: "No user found."})
        } 
        else {
            console.log(foundUser);
            // if we find a user, founder user contains the doc matched the input email
            // check if the input password from req.body matches the hashed password in our foundUser document
            /*
                bcrypt.compareSynce(<inputString>, <hashedString>)

                If the inputString and hasedString matches, compareSync method returns true otherwise return false
            */
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
            // console.log(isPasswordCorrect);
            
            //create key/token for our user if password is correct else send message
            if (isPasswordCorrect) {
                // console.log("We will create a token for the user if the password is coorect");

                // auth.createAccessToken receives our found User document as an argument, gets only necessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be send to client 
                // auth.createAccessTken is from Auth.JS
                
            
                /* 
                    To create a "key" we need to create module called auth.js 
                    This module create encoded string which contains our user's details
                    This is encoded string is what we called JSONWebToken or JWT
                    JWT can only properly unwrapped or decoded with our own secret word/string
                */
                
                return res.send({accessToken: auth.createAccessToken(foundUser)});
            }
            else {
                return res.send({message: "Incorrect Password"});
            }
        }
    })
}

module.exports.checkEmail = (req, res) => {
    // console.log(req.body);
    User.findOne({email: req.body.email})
    .then(result => {

        // findOne will return null if no match is found
        // Send false if email does not exist
        // Send true if email exists

        if (result === null) {
            return res.send(false);
        }
        else {
            return res.send(true);
        }
    })
    .catch(error => res.send(error)) 
}

module.exports.enroll = async (req, res) => {
    // check Id of the user who will enroll
    //console.log(req.user.id);

    // check the ID of the course we want to enroll
    //console.log(req.body.courseId);

    // Validate if the user is admin or not
    if (req.user.isAdmin) {
        return res.send({message: "Action Forbidden"})
    }
    /*
        Enrollemnt will come in 2 steps
        1st: Find the user who's enrolling and update his enrollment document array.
        We will push the courseID in the enrollment array
        2nd: Find the course where we are enrolling and update its enrollees subdocuments array. We will push the userID in the enrolles array

        Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting JS continue line per line

        Async and await 
        async keyword is added to a function to make our function asynchrounous. Which means that instead of JS regular behavior if running each code line by line we will be able to wait for the result of a function

        To be able to wait for the result of a function, we use the await keyword. The await keyword allow sus to wait for the function to finish and get a result before proceeding
    */

        // return a boolean to our isUserUpdated var to determine the result of the query and if we were able to save the courseId into our user's enrollment

    let isUserUpdated = await User.findById(req.user.id).then(user => {
        //console.log(user); // check if user doc is found

        // Add the courseId in an object and push that objet into the user's enrollments
        // Because we have  to follow the scheme of the enrollments subdoc array

        let newEnrollment = {
            courseId: req.body.courseId
        }

        // access the enrollment array from our user and push the new enrollment subdoc  into the enrollment array
        // We must save the user document
        user.enrollments.push(newEnrollment);

        // save user doc and return the value of the saved doc
        // return true if pushed successfully, otherwise catch and return error message

        return user.save().then(user => true).catch(err => err.message)
    });

    // If user was able to enroll properly, isUserUpdated contains true
    // Else, isUserUpdated will contain an error message
    //console.log(isUserUpdated);

    // Add an if statement and stop the process if isUserUpdated DOES NOT contain true

    if (isUserUpdated !== true) {
        return res.send({message: isUserUpdated});
    }

    // Find the course where we will enroll or add the user as an enrollee and return true IF we're able to push the user info into the enrollees array properly or send the error message instead

    let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
        // console.log(course); // contains the course we want to enroll

        // create an object to pushed into the subdocument array, enrollees
        // we have to follow the schema of our subdoc array

        let enrollee = {
            userId: req.user.id
        }

        // push the enrollee into the enrollees subdoc array of the course
        course.enrollees.push(enrollee);

        // save the course document
        // return true IF we are able to save and add the user as enrollee

        return course.save().then(course => true).catch(err => err.message);
    });

    //console.log(isCourseUpdated);
    // IF isCourseUpdated does not contain true, end the error and stop they
    if (isCourseUpdated !== true) {
        return res.send({message: isCourseUpdated})
    }

    // Ensure that we were able to both update the user and course document to add our enrollment and enrolle respectively and send a message to the client to end the enrollment process

    if (isUserUpdated && isCourseUpdated) {
        return res.send({message: "Thank you for enrolling!"})
    }
}